vec3
=====

rust vector 3 functions

```rust
extern crate vec3;

fn main() {
    let a = [1, 1, 1];
    let b = [1, 1, 1];
    let mut out = vec3::new(0, 0, 0);

    vec3::add(&mut out, &a, &b);

    assert_eq!(out, [2, 2, 2]);
}
```

use number_traits::Num;

#[inline(always)]
pub fn new<T: Copy + Num>(x: T, y: T, z: T) -> [T; 3] {
    [x, y, z]
}
#[inline(always)]
pub fn create<T: Copy + Num>(x: T, y: T, z: T) -> [T; 3] {
    new(x, y, z)
}
#[test]
fn test_new() {
    let v = new(1, 2, 3);
    assert!(v[0] == 1);
    assert!(v[1] == 2);
    assert!(v[2] == 3);
}

#[inline(always)]
pub fn clone<T: Copy + Num>(v: &[T; 3]) -> [T; 3] {
    new(v[0], v[1], v[2])
}

#[inline]
pub fn copy<'a, T: Copy + Num>(out: &'a mut [T; 3], a: &[T; 3]) -> &'a mut [T; 3] {
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out
}
#[test]
fn test_copy() {
    let mut v = [0, 0, 0];
    copy(&mut v, &[1, 2, 3]);
    assert!(v == [1, 2, 3]);
}

#[inline]
pub fn from_vec2<'a, T: Copy + Num>(out: &'a mut [T; 3], v: &[T; 2]) -> &'a mut [T; 3] {
    out[0] = v[0];
    out[1] = v[1];
    out
}
#[inline]
pub fn from_vec4<'a, T: Copy + Num>(out: &'a mut [T; 3], v: &[T; 4]) -> &'a mut [T; 3] {
    out[0] = v[0];
    out[1] = v[1];
    out[2] = v[2];
    out
}
